use daemon_rs::SOCKET_PATH;

use std::fs;
use std::io::{self, BufRead, BufReader};
use std::os::unix::net::{UnixListener, UnixStream};
use std::path::Path;
use std::thread;

fn handle_client(stream: UnixStream) {
    let stream = BufReader::new(stream);
    for line in stream.lines() {
        println!("{}", line.unwrap());
    }
    println!("no more messages from client");
}

fn main() -> io::Result<()> {
    let socket = Path::new(SOCKET_PATH);
    fs::remove_file(socket).ok();
    let listener = UnixListener::bind(socket)?;

    println!("listening for connections");

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                // connection succeeded
                println!("connection succeeded");
                thread::spawn(|| handle_client(stream));
            }
            Err(_err) => {
                // connection failed
                break;
            }
        }
    }

    Ok(())
}
