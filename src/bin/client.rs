use daemon_rs::SOCKET_PATH;

use std::io;
use std::io::Write;
use std::os::unix::net::UnixStream;

fn main() -> io::Result<()> {
    let mut stream = UnixStream::connect(SOCKET_PATH).unwrap();

    let message = "Hello World from socket client!";
    stream.write_all(message.as_bytes()).unwrap();

    Ok(())
}
